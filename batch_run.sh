#!/usr/bin/env bash
for conf_i in 1 2; do
    for i in {1..10}; do

        conf_filename="traffic_scenario_$conf_i.conf"
        suffix="$1_S0$conf_i"
        ./venv/bin/python3 ./main.py --traffic-config ./"$conf_filename" --output-suffix "$suffix"
    done
done
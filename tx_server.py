import datetime
import re
import shlex
import subprocess

import pytz
from flask import Flask, request, jsonify

app = Flask(__name__)


@app.route('/hello')
def hello_world():
    return 'Hello, World!'


@app.route('/tx')
def launch_tx():

    tx = request.args.to_dict()

    try:
        i = int(tx['i'])
        duration = int(tx['duration'])
        dst = tx['dst']
        bw = tx['bw'] if tx['bw'] != '0' else None

        cmd = 'iperf -c {} -i 1 -t {}'.format(dst, duration)
        if bw is not None:
            cmd += ' -b {}'.format(bw)

        print('Launching TX #{:02d}: {}...'.format(i, cmd))

        tx_stats = []
        with subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='utf-8') as proc:

            while True:

                line = proc.stdout.readline()
                retcode = proc.poll()

                if line == '' and retcode is not None:
                    break
                if line:

                    line_timestamp = datetime.datetime.now(pytz.UTC)
                    pattern = r'([\d.]+)\s?((?:g|m|k)?bits)\/sec'
                    match = re.search(pattern, line, re.IGNORECASE)

                    if match:

                        factors = {
                            'g': 1e9,
                            'm': 1e6,
                            'k': 1e3
                        }
                        unit_i = match.group(2).lower()[0]
                        rate = float(match.group(1)) * factors.get(unit_i, 1)
                        stats_entry = {
                            'timestamp': int(line_timestamp.timestamp()),
                            'rate': rate
                        }
                        tx_stats.append(stats_entry)

            if retcode:
                raise subprocess.CalledProcessError(retcode, proc.args, output=proc.stdout, stderr=proc.stderr)

        print('Finished TX #{:02d}: {}...'.format(i, tx))
    except (KeyError, ValueError, subprocess.CalledProcessError, FileNotFoundError) as e:
        return jsonify(str(e)), 500

    return jsonify(tx_stats), 200


app.run(
    host='0.0.0.0',
    port=8080
)

import argparse
import csv
import datetime
import json
import logging
import queue
import socket
import threading
import time

import pytz as pytz
import requests

from sflow_collector import sFlow, sflow_process_data, sflow_dump_results


class MonitoringApp:

    app_modules = []
    config = None

    def __init__(self, config):

        type(self).config = config

        self.logger = logging.getLogger('testbed')
        self.logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler = logging.StreamHandler()
        handler.setLevel(logging.DEBUG)
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

    def run(self):

        self.logger.debug('Starting app...')

        threads = []
        for module_cls in self.app_modules:

            module = module_cls()
            thread = threading.Thread(target=module.run)
            thread.start()
            threads.append(thread)

        for t in threads:
            t.join()

        self.logger.debug('Terminating app...')


class AppModule(type):

    def __new__(mcs, name, bases, attrs):

        attrs['name'] = name
        attrs['tx_queue'] = queue.Queue()
        attrs['rx_queue'] = queue.Queue()

        new_cls = super().__new__(mcs, name, bases, attrs)
        MonitoringApp.app_modules.append(new_cls)

        return new_cls


class BaseAppModule:

    name = None
    tx_queue = None
    rx_queue = None

    def __init__(self):

        assert type(self).name is not None
        self.logger = logging.getLogger('testbed.{}'.format(type(self).name))
        self.logger.debug('{} initialized.'.format(type(self).name))


class CoreAppModule(BaseAppModule, metaclass=AppModule):

    def run(self):

        data_queue = self.rx_queue
        assert isinstance(data_queue, queue.Queue)

        running = True
        tx_started = False
        tx_terminated = False

        main_collector_stats = []
        iperf_stats = []
        while running:

            data = data_queue.get(block=True)

            src = data.get('from')
            if src == TrafficGeneratorModule.name:

                msg = data.get('msg')
                tx_started = (msg == 'started' or tx_started)
                tx_terminated = (msg == 'terminated' or tx_terminated)

                if msg == 'stats':

                    samples = data.get('stats', [])
                    assert isinstance(samples, list)
                    iperf_stats.extend(samples)

            elif src == SFlowCollectorModule.name:

                msg = data.get('msg')

                if msg == 'stats' and tx_started and not tx_terminated:

                    sample = data.get('stats', {})
                    main_collector_stats.append(sample)

            else:
                self.logger.warning('Unknown message source: {}'.format(src))

            running = not tx_terminated

        self.logger.debug('Saving stats...')

        output_suffix = '{}_'.format(MonitoringApp.config.output_suffix) if MonitoringApp.config.output_suffix else ''
        output_timestamp = datetime.datetime.now(pytz.UTC).strftime('%Y%m%d_%H%M%S')
        filename = 'monitoring_{}{}.json'.format(output_suffix, output_timestamp)
        with open(filename, 'w') as f:
            json.dump(main_collector_stats, f)

        filename = 'iperf_{}{}.json'.format(output_suffix, output_timestamp)
        with open(filename, 'w') as f:
            json.dump(iperf_stats, f)

        # notify collector
        SFlowCollectorModule.rx_queue.put({
            'from': self.name,
            'msg': 'terminate'
        })

        self.logger.debug('Terminating core...')


class SFlowCollectorModule(BaseAppModule, metaclass=AppModule):

    def run(self):

        ewma_value = MonitoringApp.config.ewma
        data_queue = CoreAppModule.rx_queue
        self.run_collector(alpha_ewma=ewma_value, data_queue=data_queue)

    def run_collector(self, data_queue, ip='0.0.0.0', port=16343, alpha_ewma=0.3):

        # socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setblocking(False)
        sock.bind((ip, port))

        # check EWMA value
        if 0 <= alpha_ewma <= 1:
            self.logger.debug("EWMA: using alpha = {}".format(alpha_ewma))
        else:
            raise ValueError("EWMA: unacceptable value of alpha = {}".format(alpha_ewma))

        flows_dict = {}
        timestamp = time.time()
        sampling_ratio = 0

        while True:

            # try to read queue first
            try:
                data = self.rx_queue.get(False)

                src = data.get('from')
                if src == CoreAppModule.name:

                    if data.get('msg') == 'terminate':
                        break
            except queue.Empty:
                pass

            # ...then try to read the socket
            try:
                # 1386 bytes is the largest possible sFlow packet, by spec 3000 seems to be the number by practice
                data, addr = sock.recvfrom(3000)
            except BlockingIOError:
                time.sleep(0.01)
                continue

            # in case of data received by the socket, process retrieved sFlow packet
            sFlowData = sFlow(data)
            sampling_ratio = sflow_process_data(sFlowData, flows_dict, sampling_ratio)

            # dump data or wait for more
            if time.time() > timestamp + 1:

                timestamp = int(time.time())
                sflow_dump_results(flows_dict, timestamp, sampling_ratio, alpha_ewma, data_queue, self.name)


class TrafficGeneratorModule(BaseAppModule, metaclass=AppModule):

    def run(self):

        # load txs config
        config_filename = MonitoringApp.config.traffic_config
        txs = []
        with open(config_filename, 'r') as f:

            reader = csv.reader(f)
            for r in reader:

                tx = {
                    'at': int(r[0]),
                    'src': r[1],
                    'dst': r[2],
                    'duration': int(r[3]),
                    'bw': r[4],
                }
                txs.append(tx)

        # sort txs by timestamps
        txs.sort(key=lambda x: x['at'])

        hosts = set([tx['src'] for tx in txs] + [tx['dst'] for tx in txs])
        for host in hosts:

            url = 'http://{}:8080/hello'.format(host)
            try:
                response = requests.get(url, timeout=5)
                response.raise_for_status()
            except (requests.exceptions.ConnectionError, requests.HTTPError):
                self.logger.error('Host {} not responding ({}).'.format(host, url))
            else:
                self.logger.debug('Host {} OK.'.format(host))

        # start traffic generation
        self.logger.debug('Starting TX generator...')
        start_timestamp = datetime.datetime.now(pytz.UTC)

        # notify core
        CoreAppModule.rx_queue.put({
            'from': self.name,
            'msg': 'started'
        })

        # dispatch subsequent txs
        ths = []
        for i, tx in enumerate(txs):

            tx['i'] = i

            while True:

                timestamp = datetime.datetime.now(pytz.UTC)
                if int(timestamp.timestamp() >= start_timestamp.timestamp() + tx['at']):
                    break
                else:
                    time.sleep(0.01)

            self.logger.debug('T+{:03d}, requesting TX #{:03d}: {}...'.format(int((timestamp - start_timestamp).total_seconds()), i, tx))

            th = threading.Thread(target=self.request_tx, kwargs={'i': i, 'tx': tx})
            th.start()
            ths.append(th)

        self.logger.debug('All TXs dispatched...')

        for th in ths:
            th.join()

        # notify core (all requests finished)
        CoreAppModule.rx_queue.put({
            'from': self.name,
            'msg': 'terminated'
        })

        self.logger.debug('Terminating TX generator...')

    def request_tx(self, i, tx):

        try:
            url = 'http://{}:{}/tx'.format(tx['src'], 8080)
            response = requests.get(
                url=url,
                params=tx
            )
            response.raise_for_status()
        except requests.exceptions.ConnectionError:
            self.logger.error('TX #{:03d} failed: ConnectionError.'.format(i))
        except requests.HTTPError:
            try:
                self.logger.error('TX #{:03d} request failed with code {} ({}).'.format(i, response.status_code, response.content.decode()))
            except NameError:
                self.logger.error('TX #{:03d} request failed.'.format(i))
        else:

            tx_stats = response.json()
            CoreAppModule.rx_queue.put({
                'from': self.name,
                'msg': 'stats',
                'stats': tx_stats
            })
            self.logger.debug('TX #{:03d} request succeeded.'.format(i))


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--traffic-config', type=str, default='traffic.conf',
                        help='Path to traffic config file.')
    parser.add_argument('--output-suffix', type=str, default=None,
                        help='Output file suffix.')
    parser.add_argument('--ewma', type=float, default=0.3,
                        help='EWMA value.')

    args = parser.parse_args()

    app = MonitoringApp(args)
    app.run()

# Configuration

## cloud environment

- use desired zone
- remember to allow traffic via security group
- default username for Amazon Linux is `ec2-user`
- run iperf server on each node with `iperf -s`

## iperf

    sudo yum -y install  https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm  && sudo yum -y install iperf

## hsflowd 

### prerequisites
    
    sudo yum -y install libcap-devel libpcap-devel

### hsflow daemon
   
    wget https://github.com/sflow/host-sflow/releases/download/v2.0.26-3/hsflowd-centos8-2.0.26-3.x86_64.rpm
    sudo yum -y install hsflowd-centos8-2.0.26-3.x86_64.rpm
    sudo vim /etc/hsflowd.conf
    sudo service hsflowd restart

### hsflowd.conf

    sflow {
        polling=M
        sampling=N
        collector { ip = X.X.X.X udpport = Y }
        collector { ip = X.X.X.X udpport = Z }
        pcap { dev=eth0 }
    }
    
for example
   
    sflow {
        polling=30
        sampling=100
        collector { ip = 172.30.0.1 udpport = 16343 }
        collector { ip = 172.30.0.1 udpport = 6343 }
        pcap { dev=eth0 }
    }
    
### sFlow-RT

    wget https://inmon.com/products/sFlow-RT/sflow-rt.tar.gz
    tar -xvzf sflow-rt.tar.gz
    ./sflow-rt/get-app.sh sflow-rt flow-trend
    ./sflow-rt/get-app.sh sflow-rt top-flows

    
## monitoring app & iperf agent

### git repo

    sudo yum -y install git python3
    git clone https://m_r@bitbucket.org/m_r/cloud-monitoring-sflow-2020.git
    cd cloud-monitoring-sflow-2020/
    python3 -m venv venv
    ./venv/bin/pip3 install -r ./requirements.txt
    
### run monitoring module
    
Single run:

    ./venv/bin/python3 ./main.py
    
Batch mode:

    ./batch_run.sh <output_file_suffix>
    
### run tx server (iperf agent)

    ./venv/bin/python3 ./tx_server.py
    
### run sFlow-RT (optional)

    ./sflow-rt/start.sh
    
### change sampling rate 

    N=200; FILE=/etc/hsflowd.conf; sudo sed -i -r "s/(sampling=)([0-9]+)/\1$N/" "$FILE" && sudo service hsflowd restart && sudo service hsflowd status && cat "$FILE"  
    
## cloud environment

Use to deploy cloud environment automatically.

1. In the Vocareum Sandbox Environment, click Details then AWS->Show, then AWS CLI->Show, reveal AWS CLI credentials.
2. Install AWS CLI: https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html
3. Configure credentials retrieved from Vocareum using `aws configure` (you may need to manually edit `~/.aws/credentials`)
4. Install Terraform: https://learn.hashicorp.com/tutorials/terraform/install-cli?in=terraform/aws-get-started.
5. Enter the `aws/` directory.
6. Run `terraform init` (first time), then `terraform apply`. The SSH key and instance IPs will be printed out.
    * To run an alternative config, provide variable file, e.g. `terraform apply --var-file=./alternative_suse.tfvars`.
7. Use SSH key to connect to the instances.
    * Use `terraform output ssh_key > key.pem` to save SSH key.
8. The environment is ready to use.
9. Run `terraform destroy` to terminate the environment.

    

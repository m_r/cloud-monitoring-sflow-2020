#!/bin/bash
zypper ref
zypper -n in git python3 java

cd /home/ec2-user

wget https://inmon.com/products/sFlow-RT/sflow-rt.tar.gz
tar -xvzf sflow-rt.tar.gz
/home/ec2-user/sflow-rt/get-app.sh sflow-rt flow-trend
/home/ec2-user/sflow-rt/get-app.sh sflow-rt top-flows

git -C /home/ec2-user clone https://m_r@bitbucket.org/m_r/cloud-monitoring-sflow-2020.git
cd /home/ec2-user/cloud-monitoring-sflow-2020/
python3 -m venv venv
./venv/bin/pip3 install -r ./requirements.txt
chown ec2-user:users /home/ec2-user -R
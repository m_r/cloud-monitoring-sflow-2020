#!/bin/bash
yum update
yum -y install  https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm  && sudo yum -y install iperf
yum -y install libcap-devel libpcap-devel
yum -y install git python3

wget https://github.com/sflow/host-sflow/releases/download/v2.0.26-3/hsflowd-centos8-2.0.26-3.x86_64.rpm
yum -y install hsflowd-centos8-2.0.26-3.x86_64.rpm

echo "
sflow {
    polling=${sflow_polling}
    sampling=${sflow_sampling}
    collector { ip = ${collector_ip_private} udpport = 16343 }
    collector { ip = ${collector_ip_private} udpport = 6343 }
    pcap { dev=eth0 }
}
" > /etc/hsflowd.conf
systemctl enable hsflowd
systemctl start hsflowd

cd /home/ec2-user

git clone https://m_r@bitbucket.org/m_r/cloud-monitoring-sflow-2020.git
cd cloud-monitoring-sflow-2020/
python3 -m venv venv
./venv/bin/pip3 install -r ./requirements.txt
chown ec2-user:ec2-user . -R
#!/bin/bash
zypper ref
zypper -n in make gcc gcc-c++ automake insserv
zypper -n in dbus-1-devel openssl-devel
zypper -n in libcap-devel libpcap-devel
zypper -n in git python3

cd /tmp
wget https://nav.dl.sourceforge.net/project/iperf2/iperf-2.0.14a.tar.gz
tar xzvf iperf-2.0.14a.tar.gz
cd iperf-2.0.14a/
./configure
make
make install

cd /tmp
wget https://github.com/sflow/host-sflow/archive/v2.0.30-1.tar.gz -O hsflowd.tar.gz
tar xzvf hsflowd.tar.gz
cd host-sflow-2.0.30-1/
make FEATURES="HOST PCAP TCP SYSTEMD DBUS"
make install
make schedule

echo "
sflow {
    polling=${sflow_polling}
    sampling=${sflow_sampling}
    collector { ip = ${collector_ip_private} udpport = 16343 }
    collector { ip = ${collector_ip_private} udpport = 6343 }
    pcap { dev=eth0 }
}
" > /etc/hsflowd.conf
systemctl enable hsflowd
systemctl restart hsflowd

git -C /home/ec2-user/ clone https://m_r@bitbucket.org/m_r/cloud-monitoring-sflow-2020.git
cd /home/ec2-user/cloud-monitoring-sflow-2020/
python3 -m venv venv
./venv/bin/pip3 install -r ./requirements.txt
chown ec2-user:users . -R
#!/bin/bash
yum -y install git python3 java

cd /home/ec2-user

wget https://inmon.com/products/sFlow-RT/sflow-rt.tar.gz
tar -xvzf sflow-rt.tar.gz
./sflow-rt/get-app.sh sflow-rt flow-trend
./sflow-rt/get-app.sh sflow-rt top-flows

git clone https://m_r@bitbucket.org/m_r/cloud-monitoring-sflow-2020.git
cd cloud-monitoring-sflow-2020/
python3 -m venv venv
./venv/bin/pip3 install -r ./requirements.txt
chown ec2-user:ec2-user /home/ec2-user -R
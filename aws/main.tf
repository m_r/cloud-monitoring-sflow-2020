terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.70"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "eu-central-1"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "ami" {
  default = "ami-00a205cb8e06c3c4e"
}

variable "sflow_sampling" {
  default = "100"
}

variable "sflow_polling" {
  default = "30"
}

variable "cloud_init_template_suffix" {
  default = "aml2"
}

variable "collector_private_ip" {
  default = "172.30.100.100"
}

variable "host_a_private_ip" {
  default = "172.30.100.101"
}

variable "host_b_private_ip" {
  default = "172.30.100.102"
}

resource "aws_security_group" "monitoring_default" {

  name        = "monitoring_default"
  description = "Allow SSH from outside, all within VPC."
  vpc_id = aws_vpc.vpc-1.id

  ingress {
    description = "sFlow-RT panel from anywhere"
    from_port   = 8008
    to_port     = 8008
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH from anywhere"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "All traffic within VPC"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    self = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "collector" {

  ami           = var.ami
  instance_type = var.instance_type
  vpc_security_group_ids = [
    aws_security_group.monitoring_default.id]
  user_data = data.template_cloudinit_config.collector_init.rendered
  key_name = aws_key_pair.ec2_key.key_name
  subnet_id = aws_subnet.subnet-1.id
  private_ip = var.collector_private_ip
  associate_public_ip_address = true

  depends_on = [
    aws_internet_gateway.gw-1
  ]

  tags = {
    Name = "monitoring-collector"
  }
}

resource "aws_instance" "host-a" {

  ami           = var.ami
  instance_type = var.instance_type
  vpc_security_group_ids = [
    aws_security_group.monitoring_default.id]
  user_data = data.template_cloudinit_config.host_init.rendered
  key_name = aws_key_pair.ec2_key.key_name
  subnet_id = aws_subnet.subnet-1.id
  private_ip = var.host_a_private_ip
  associate_public_ip_address = true

  depends_on = [
    aws_internet_gateway.gw-1
  ]

  tags = {
    Name = "monitoring-host-a"
  }
}

resource "aws_instance" "host-b" {

  ami           = var.ami
  instance_type = var.instance_type
  vpc_security_group_ids = [
    aws_security_group.monitoring_default.id]
  user_data = data.template_cloudinit_config.host_init.rendered
  key_name = aws_key_pair.ec2_key.key_name
  subnet_id = aws_subnet.subnet-1.id
  private_ip = var.host_b_private_ip
  associate_public_ip_address = true

  depends_on = [
    aws_internet_gateway.gw-1
  ]

  tags = {
    Name = "monitoring-host-b"
  }
}

resource "aws_vpc" "vpc-1" {
  cidr_block = "172.30.0.0/16"

  tags = {
    Name = "vpc-1"
  }
}

resource "aws_subnet" "subnet-1" {
  cidr_block = "172.30.100.0/24"
  vpc_id = aws_vpc.vpc-1.id
}

resource "aws_route_table" "route-table-1" {
  vpc_id = aws_vpc.vpc-1.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw-1.id
  }

  tags = {
    Name = "route-table-1"
  }
}

resource "aws_route_table_association" "rt-assoc" {
  route_table_id = aws_route_table.route-table-1.id
  subnet_id = aws_subnet.subnet-1.id
}

resource "aws_internet_gateway" "gw-1" {

  vpc_id = aws_vpc.vpc-1.id

  tags = {
    Name = "gw-1"
  }
}

data "template_cloudinit_config" "collector_init" {

  gzip          = false
  base64_encode = false

  part {
    filename     = "init.sh"
    content_type = "text/x-shellscript"
    content = file("${path.module}/cloud_init_collector_${var.cloud_init_template_suffix}.sh")
  }
}

data "template_cloudinit_config" "host_init" {

  gzip          = false
  base64_encode = false

  part {
    filename     = "init.sh"
    content_type = "text/x-shellscript"
    content = templatefile("${path.module}/cloud_init_host_${var.cloud_init_template_suffix}.sh", {
      collector_ip_private = aws_instance.collector.private_ip
      sflow_sampling = var.sflow_sampling
      sflow_polling = var.sflow_polling
    })
  }
}

resource "tls_private_key" "ssh_key" {
  algorithm   = "RSA"
}

resource "aws_key_pair" "ec2_key" {
  key_name   = "developer-key"
  public_key = tls_private_key.ssh_key.public_key_openssh
}

output "ssh_key" {
  value = tls_private_key.ssh_key.private_key_pem
}

output "collector_ip" {
  value = aws_instance.collector.public_ip
}

output "host_a_ip" {
  value = aws_instance.host-a.public_ip
}

output "host_b_ip" {
  value = aws_instance.host-b.public_ip
}
